import React, { useState, useEffect } from "react";
import axios from 'axios';

const App = () => {
  const [products, setProducts] = useState(null);
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')

  useEffect(() => {
    if(!products) {
      getProducts();
    }
  })

  const getProducts = async () => {
    let result = await axios.get('/api/product');
    console.log(result.data || []);
    setProducts(result.data || []);
  }

  const renderProduct = product => {
    return (
      <li key={product._id}>
        <h3>{product.name}</h3>
        <p>{product.description}</p>
      </li>
    );
  };

  const handleNameChange = (event) => {
    setName([event.target.value])
  }

  const handleDescriptionChange = (event) => {
    setDescription([event.target.value])
  }

  const handleSubmit = (event) => {
    //alert(`${name} ${description}`)
    event.preventDefault() //stops browser clearing form
    const product = {
      "name": `${name}`,
      "description": `${description}`
    }
    axios.post('/api/product', product); 
    getProducts()
    renderProduct(product)
  }
  
  return (
    <div className="App">
      <ul className="list">
        {(products && products.length > 0) ? (products.map(product => renderProduct(product))) : (
          <p>No products found</p>
        )}
      </ul>
        <form onSubmit={handleSubmit}> 
          <label>Name:     </label>
          <input type="text" onChange={handleNameChange} required></input>
          <label>Description: </label>
          <input type="text" onChange={handleDescriptionChange} required></input><br/>
          <button type='submit'>OK</button>
        </form>
    </div>
  );
}

export default App;

/*
 *  This version supports a POST request to add a product to the collection.  All it does is provide a Add Mango
 *  button, which when pushed will send a POST request with a mango.  This version needs some finessing - add
 *  a form, then re-render the product list on display.
 * 
 *  See changes in server.js to support a POST request and retrieve the body of the request.
 * 
 *  nicnei@gmail.com
 */